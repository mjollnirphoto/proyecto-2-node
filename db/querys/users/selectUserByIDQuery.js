const getDB = require('../../db');

const { generateError } = require('../../../helpers');

const selectUserByIdQuery = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();
    const { id } = req.params;

    const [users] = await connection.query(
      `SELECT id, name, email, createdAt FROM users WHERE id = ?`,
      [id]
    );

    if (users.length < 1) {
      generateError('Usuario no encontrado', 404);
    }
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = selectUserByIdQuery;
