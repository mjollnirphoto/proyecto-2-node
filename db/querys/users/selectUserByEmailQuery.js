const getDB = require('../../db');

const { generateError } = require('../../../helpers');

const selectUserByEmailQuery = async (email) => {
  let connection;

  try {
    connection = await getDB();

    const [users] = await connection.query(
      `SELECT id, password, name FROM users WHERE email = ?`,
      [email]
    );

    if (users.length < 1) {
      generateError('Usuario no encontrado', 404);
    }

    return users[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = selectUserByEmailQuery;
