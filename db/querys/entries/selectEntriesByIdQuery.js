const getDB = require('../../db');

const { generateError } = require('../../../helpers');

const selectEntriesByIdQuery = async (identries) => {
  let connection;

  try {
    connection = await getDB();

    const [entries] = await connection.query(
      `SELECT * FROM entries WHERE id = ?`,
      [identries]
    );

    if (entries.length < 1) {
      generateError('Entrada no encontrada', 404);
    }

    return entries[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = selectEntriesByIdQuery;
