const getDB = require('../../db');

const selectAllEntriesQuery = async (keyword = '') => {
  let connection;

  try {
    connection = await getDB();

    const [entries] = await connection.query(
      `SELECT * FROM entries WHERE url LIKE ? ORDER BY createdAt DESC`,
      [`%${keyword}%`]
    );

    return entries;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = selectAllEntriesQuery;
