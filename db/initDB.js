'use strict';

require('dotenv').config();

const getDB = require('./db');

let connection;

async function main() {
  try {
    connection = await getDB();

    console.log('Borrando tablas');

    await connection.query('DROP TABLE IF EXISTS votes');
    await connection.query('DROP TABLE IF EXISTS entries');
    await connection.query('DROP TABLE IF EXISTS users');

    console.log('Creando tablas');

    await connection.query(`
    CREATE TABLE users (
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(100),
        email VARCHAR(100) UNIQUE NOT NULL,
        password VARCHAR(100) NOT NULL,
        createdAt TIMESTAMP NOT NULL);
    `);

    await connection.query(`
    CREATE TABLE entries (
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        idUser INT UNSIGNED NOT NULL,
        url VARCHAR(100) NOT NULL,
        title VARCHAR(100) NOT NULL,
        description VARCHAR(255) NOT NULL,
        createdAt TIMESTAMP NOT NULL,
        FOREIGN KEY (idUser) REFERENCES users(id) );
    `);

    await connection.query(`
    CREATE TABLE votes (
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        votes INT NOT NULL,
        idUser INT UNSIGNED NOT NULL,
        idEntry INT UNSIGNED NOT NULL,
        createdAt TIMESTAMP NOT NULL,
        FOREIGN KEY (idUSer) REFERENCES users(id),
        FOREIGN KEY (idEntry) REFERENCES entries(id) )
    `);

    console.log('Tablas creadas');
  } catch (error) {
    console.error('ERROR:', error.message);
    process.exit(1);
  } finally {
    if (connection) {
      connection.release();
    }
    process.exit(0);
  }
}

main();
