###### App para compartir enlaces

Implementar una API que permita a los usuarios registrarse y compartir enlaces web que
consideren interesantes. Otros usuarios podrán votarlos si les gustan.

### Instalar 💿

- `npm init -y`✅ Para crear el package.json 📄
- `npm install minimist chalk sharp `....✅ Instalamos los modulos y nos crea node_modules ... 📄
- Creo el archivo `.env.example` ✅ como `.env` 📄 y cubrir los datos necesarios.

- Creo el archivo `.prettierrc.json` 📄 con los datos ✅ :
  {
  "trailingComma": "es5",
  "tabWidth": 2,
  "semi": true,
  "singleQuote": true
  }
- Creo el archivo `.eslintrc.json` 📄 con los datos ✅ :
  {
  "env": {
  "commonjs": true,
  "es2021": true,
  "node": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
  "ecmaVersion": "latest"
  },
  "rules": {
  "no-unused-vars": "off"
  }
  }
- Creamos `.gitignore` y le metemos node_modules y .env ✅
- Creamos `.env.example` con los mismos campos que `.env` pero vacios para cada usuario. ✅
- Crear una base de datos vacía en una instancia de MySQL local.✅
- Ejecutar `npm run initDB` para crear las tablas necesarias en la base de datos anteriormente creada.✅
- Ejecutar `npm run dev` o `npm start` para lanzar el servidor.✅

### Módulos de npm

- Instalar las dependencias de produción : dotenv ✅ express ✅
  `npm i dotenv express`✅
- Instalar las dependencias de desarrollo:
  `npm i eslint prettier nodemon morgan -D`
  eslint( Inicializo eslint: `npx eslint --init`) ✅ prettier ✅ nodemon ✅ morgan ✅
  mysql2 (`npm i mysql2` Instalar mySql) ✅
  jsonwebtoken ✅ bcrypt ✅
  chalk ✅

## Entidades o TABLAS

- Users:✅

  - id
  - name
  - email
  - password
  - createdAt

- Entries:✅

  - id
  - idUser
  - url
  - title
  - description
  - createdAt

- Votes:✅

  - id
  - votos
  - idUser
  - idEntry
  - createdAt

- Podremos comprobar en **Mysql Workbench** todas las creaciones o cambios . ✅

## Endpoints

- Con **Postman** iremos comprobando que funcionen todas las peticiones(request). ✅

# Usuarios:

- POST - Registro de usuario. ✅
- POST - Login de usuario (devuelve token). ✅
- GET - Devuelve información del usuario del token. **TOKEN** ✅
- PATCH - Editar la contraseña de un usuario | Solo el propio usuario necesita TOKEN ✅
- PATCH - /users/:id - Editar un usuario (nombre, email, password) | Solo el propio usuario ✅

# Entries:

- POST - /entries - publicar un enlace (url, title y description) | Token obligatorio ✅
- GET - /entries/:id - ver los enlaces publicados en el dia de hoy y anteriores | Sin token ✅
- DELETE - /entries/:id - borra un enlace | Token obligatorio ✅
- POST - /entries/:id/votes - vota una entrada | Token obligatorio ✅

## MIDDLEWARES

- Middlewares de ERROR ( encima del de Ruta ) ✅
- Middlewares de ruta SIEMPRE antes del `app.listen` ✅

- Comprueba que el usuario pasado por path params exista ✅
- Errores ✅
