'use strict';

require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const chalk = require('chalk'); // version 4.1.2 porque es la que funciona perfecta

console.log(
  chalk.bgCyan(
    'Entre libremente por su propia voluntad y deje parte de la felicidad que trae ...'
  )
);

// Creamos las rutas de usuario
const {
  newUser,
  loginUser,
  getOwnUser,
  modifyPassword,
  modifyEmail,
  modifyName,
} = require('./controllers/users');

// Creamos las rutas de entries
const {
  newEntry,
  listEntry,
  deleteEntry,
  votes,
} = require('./controllers/entries');
const insertUrlQuery = require('./db/querys/entries/insertUrlQuery');

// Middleware para generar token
const infoToken = require('./middleware/infoToken');

// Creamos una instancia de express
const app = express();

// Middleware de Morgan
app.use(morgan('dev'));

// Middleware Body
app.use(express.json());

/**
 * ########################
 * # Middleware Usuarios  #
 * ########################
 */

// Middleware para nuevo usuario
app.post('/users', newUser);

// Middleware para usuario logueado
app.post('/users/login', loginUser);

// Obtener información del usuario del token.
app.get('/users', infoToken, getOwnUser);

// Editar la contraseña de un usuario
app.patch('/users/password', infoToken, modifyPassword);

// Editar el email de un usuario
app.patch('/users/email', infoToken, modifyEmail);

// Editar el nombre de un usuario
app.patch('/users/name', infoToken, modifyName);

/**
 * ########################
 * ## Middleware Entries ##
 * ########################
 */

// Creamos una entrada
app.post('/entries', infoToken, newEntry, insertUrlQuery);

// Listamos las entradas
app.get('/entries', listEntry);

// Eliminamos una entry
app.delete('/entries/:id', infoToken, deleteEntry);

// Votamos una entrada
app.post('/entries/:id/votes', infoToken, votes);

/**
 * ########################
 * ## Middleware Errores ##
 * ########################
 */

// Middleware de gestion de errores
app.use((error, req, res, next) => {
  console.error(error);
  res.status(error.statusCode || 500).send({
    status: 'error',
    message: error.message,
  });
});

// Middleware de 404
app.use((req, res) => {
  res.status(404).send({
    status: 'error',
    message: 'Not found! 💀',
  });
});

// Escucha express
app.listen(process.env.PORT, () => {
  console.log(
    chalk.green(
      `Servidor Operativo 😎 en ▶️ http://127.0.0.1:${process.env.PORT} ◀️ 😎`
    )
  );
});
