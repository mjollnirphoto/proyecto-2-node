const newEntry = require('./newEntry');
const listEntry = require('./listEntry');
const deleteEntry = require('./deleteEntry');
const votes = require('./votes');

module.exports = { newEntry, listEntry, deleteEntry, votes };
