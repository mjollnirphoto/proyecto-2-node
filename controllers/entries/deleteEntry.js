const selectEntriesByIdQuery = require('../../db/querys/entries/selectEntriesByIdQuery');
const deleteEntriesQuery = require('../../db/querys/entries/deleteEntriesQuery');

const deleteEntry = async (req, res, next) => {
  try {
    const { id } = req.params;

    // Obtenemos la info del tweet.
    await selectEntriesByIdQuery(id);

    // Borramos el tweet.
    await deleteEntriesQuery(id);

    res.send({
      status: 'ok',
      message: 'Entrada eliminada',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = deleteEntry;
