const insertUrlQuery = require('../../db/querys/entries/insertUrlQuery');

const { generateError } = require('../../helpers');

const newEntry = async (req, res, next) => {
  try {
    const { url, title, description } = req.body;

    if (!url || !title || !description) {
      generateError('Faltan campos', 400);
    }

    // Insertamos la nueva url.
    await insertUrlQuery(url, title, description, req.userInfo.id);

    res.send({
      status: 'ok',
      message: 'Entrada creada 👍🏻',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = newEntry;
