const selectAllEntriesQuery = require('../../db/querys/entries/selectAllEntriesQuery');

const listEntry = async (req, res, next) => {
  try {
    const { keyword } = req.query;

    // Lista de tweets.
    const entries = await selectAllEntriesQuery(keyword);

    res.send({
      status: 'ok',
      data: {
        entries,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = listEntry;
