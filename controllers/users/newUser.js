const insertUserQuery = require('../../db/querys/users/selectUserQuery');

const { generateError } = require('../../helpers');

const newUser = async (req, res, next) => {
  try {
    const { email, password, name } = req.body;

    if (!email || !password) {
      generateError('Faltan campos ❌', 400);
    }

    // Insertamos usuario en la base de datos.
    await insertUserQuery(email, password, name);

    res.send({
      status: 'ok',
      message: 'Usuario creado ✅',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = newUser;
