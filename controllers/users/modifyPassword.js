'use strict';

const getDB = require('../../db/db');
const { generateError } = require('../../helpers');
const bcrypt = require('bcrypt');

const modifyPassword = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { oldPwd, newPwd } = req.body;

    const [users] = await connection.query(
      `
          SELECT id, password
          FROM users
          WHERE id=?
      `,
      [req.userInfo.id]
    );

    if (users.length === 0) {
      generateError('Usuario no encontrado', 404);
    }

    const validPass = await bcrypt.compare(oldPwd, users[0].password);

    if (!validPass) {
      generateError('Contraseña incorrecta ❌', 401);
    }

    const hashedPass = await bcrypt.hash(newPwd, 10);

    await connection.query(
      `
        UPDATE users
        SET password=?
        WHERE id=?
    `,
      [hashedPass, req.userInfo.id]
    );

    res.send({
      status: 'ok',
      message: 'Password cambiada',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = modifyPassword;
