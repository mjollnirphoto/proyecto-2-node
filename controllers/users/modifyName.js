'use strict';

const getDB = require('../../db/db');
const { generateError } = require('../../helpers');

const modifyName = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { name } = req.body;

    // Comprobamos que el usuario sea el mismo que el token
    if (!name) {
      generateError('Faltan campos', 400);
    }

    // Comprobar que no exista otro usuario con el nuevo nombre
    const [existingName] = await connection.query(
      `
        SELECT id
        FROM users
        WHERE name=?
      `,
      [name]
    );

    if (existingName.length > 0) {
      const error = new Error(
        'Ya existe un usuario con el nombre proporcionado en la base de datos'
      );
      error.statusCode = 409;
      throw error;
    }

    // Actualizar los datos finales

    await connection.query(
      `
        UPDATE users
        SET name=?
        WHERE id=?
      `,
      [name, req.userInfo.id]
    );

    res.send({
      status: 'ok',
      message: 'Nombre actualizado',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};
module.exports = modifyName;
